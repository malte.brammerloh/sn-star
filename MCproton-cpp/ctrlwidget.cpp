/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2016  <copyright holder> <reimer@cbs.mpg.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "ctrlwidget.hpp"
#include "protonsim.hpp"

#include <QVBoxLayout>
#include <QPushButton>
#include <QFileDialog>
#include <QLineEdit>

#include "maskswidget.hpp"


CtrlWidget::CtrlWidget(QString name)
{
	setWindowTitle(name);
	setLayout(new QVBoxLayout);

	doSimBtn = new QPushButton("start simulation");
	storePath = new QLineEdit(this);
	
	MasksWidget *w_base= new MasksWidget(base, storePath, this);
	layout()->addWidget(w_base);
	
	connect(doSimBtn,SIGNAL(clicked()),SLOT(doSim()));

	doSimBtn->setEnabled(false);

	layout()->addWidget(doSimBtn);
	layout()->addWidget(storePath);

}

void CtrlWidget::doSim()
{
// 	ProtonSim::return_val erg,erg_IV,erg_EV;
// 	ProtonSim sim(*Vm,*delta_B,*delta_B_base,*T2mask,*T2mask_base,*T2starmask,*T2starmask_base);
// 	std::tie(erg,erg_IV,erg_EV) =sim.simulate(10000);
	
// 	LOG(MCLog,notice) << "writing signal data to " << app.parameters["out"].as<std::string>();
// 	std::ofstream ofile(app.parameters["out"].as<std::string>());
// 	ofile << "signal_GE_base\tsignal_GE_exci\tsignal_SE_base\tsignal_SE_exci" << '\t';
// 	ofile << "signal_GE_base_IV\tsignal_GE_exci_IV\tsignal_SE_base_IV\tsignal_SE_exci_IV" << '\t';
// 	ofile << "signal_GE_base_EV\tsignal_GE_exci_EV\tsignal_SE_base_EV\tsignal_SE_exci_EV" ;
// 	ofile << std::endl;
// 	ofile << std::fixed << std::setprecision(8);
// 	for(int i=0;i<mask.ntime_step;i++){
// 		ofile << erg.signal_GE_base[i].real() << '\t' << erg.signal_GE_exci[i].real() << '\t' << erg.signal_SE_base[i].real() << '\t' << erg.signal_SE_exci[i].real() << '\t';
// 		ofile << erg_IV.signal_GE_base[i].real() << '\t' << erg_IV.signal_GE_exci[i].real() << '\t' << erg_IV.signal_SE_base[i].real() << '\t' << erg_IV.signal_SE_exci[i].real() << '\t';
// 		ofile << erg_EV.signal_GE_base[i].real() << '\t' << erg_EV.signal_GE_exci[i].real() << '\t' << erg_EV.signal_SE_base[i].real() << '\t' << erg_EV.signal_SE_exci[i].real();
// 		ofile << std::endl;
// 	}

}

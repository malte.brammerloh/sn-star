/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2016  Enrico Reimer <reimer@cbs.mpg.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "calcmask.hpp"
#include <cmath>
#include <fstream>
#include <isis/core/io_factory.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/filesystem/convenience.hpp>

#include <isis/math/fft.hpp>
#include <isis/core/io_factory.hpp>


namespace fs = boost::filesystem;
using util::fvector3;

template<int COLS, typename TYPE> std::array<std::vector<TYPE>,COLS> loadTable(fs::path fname){
	fs::ifstream file(fname);
	std::cout << "Loading " << fs::canonical(fname) << std::endl;
	std::array<std::vector<TYPE>,COLS> ret;

	size_t line=0;
	while(file){
		for(std::vector<TYPE> &column:ret){
			float dummy;
			file >> dummy;
			column.push_back(dummy);
		}
	}
	return ret;
}

std::vector< CalcMask::edge > CalcMask::loadEdges(boost::filesystem::path fname)
{ 
	fs::ifstream file(fname);
	std::cout << "Loading " << fs::canonical(fname) << std::endl;

	std::vector< CalcMask::edge > ret;

	while(file){
		float a,b;
		file >> a >> b;
		ret.push_back(edge(a,b));
	}
	return ret;
}


std::vector< util::fvector3 > CalcMask::loadVectors(boost::filesystem::path fname)
{
	fs::ifstream file(fname);
	std::cout << "Loading " << fs::canonical(fname) << std::endl;

	std::vector< fvector3 > ret;

	while(file){
		fvector3 dummy;
		file >> dummy[0] >> dummy[1] >> dummy[2];
		ret.push_back(dummy);
	}
	return ret;
}


double so2_func(float po2,species spc){
	float alpha = 1.27e-15;  // Bunsen solubility coefficient umol / um^3 / Torr
	po2=po2/alpha; // one name should be altered
	double so2;
	if (spc==human){
		if (true){
			// taken from Lobdell Am Physiol Soc 1981
			// table 2 and eq 1
			const float a = 0.34332;
			const float b = 0.64073;
			const float c = 0.34128;
			const float n = 1.58678;

			const float P50 = 26.6; // Roughton 1973

			//x = (po2/P50)*10^(0.024*(37-T) + 0.40*(pH-7.4) + 0.06 * log(40/pco2));
			float x = po2/P50;

			so2 = (a*pow(x,n) + b*pow(x,2*n))/(1 + c*pow(x,n) + b*pow(x,2*n));
		} else if (false){
			// Empirical model relating SO2 and pO2
			// Ref: http://www.ventworld.com/resources/oxydisso/dissoc.html#model
			const float a1 = -8.532e3;
			const float a2 =  2.121e3;
			const float a3 = -6.707e1;
			const float a4 =  9.360e5;
			const float a5 = -3.135e4;
			const float a6 =  2.396e3;
			const float a7 = -6.710e1;

			float so2 = (a1*po2 + a2*pow(po2,2)+a3*pow(po2,3)+pow(po2,4)) / (a4+a5*po2+a6*pow(po2,2)+a7*pow(po2,3)+pow(po2,4));
		} else if (false){
			// another source
			so2=1/((23400/(pow(po2,3)+150*po2))+1);
		}
	} else if (spc==rat){
		const float  hillC = 2.7;
		const float  p50 = 37; // Rat, Ellis C.G. et al., Am. J. Phys Heart Circ Phys 258, H1216-, 2002
		so2=pow(po2,hillC) / ( pow(po2,hillC) + pow(p50,hillC)  );
	} else if (spc==mouse) {
		const float  hillC = 2.59;
		const float  p50 = 40.2; // C57BL/6 mice, Uchida K. et al., Zoological Science 15, 703-706, 1997
		so2=pow(po2,hillC) / ( pow(po2,hillC) + pow(p50,hillC)  );
	}
	return so2; // muss man das machen?
}

template<typename OUT,typename IN,class OP> data::MemChunk<OUT> transform(const data::TypedChunk<IN> &in,const OP &op){
	util::vector4<size_t> size=in.getSizeAsVector();
	data::MemChunk<OUT> out(size[0],size[1],size[2],size[3]);
	static_cast<util::PropertyMap&>(out)=in;
	std::transform(in.begin(),in.end(),out.begin(),op);    
	return out;
}


// CalcMask::parameters::parameters(size_t nNodes, util::vector3< size_t > img_size){}


CalcMask::CalcMask(boost::filesystem::path rootpath, util::ivector3 ROI_start, util::ivector3 ROI_end, species spc, float phi_angle, float omega_angle, float B0)
{
	input_root=rootpath;
	nodeEdges=loadEdges(rootpath / "nodeEdges.txt");
	nodePos_um=loadVectors(rootpath / "nodePos_um.txt");
	nNodes=nodePos_um.size();

	params.nodeSegN=loadTable<1,int>(rootpath / "nodeSegN.txt")[0];
	params.edgeSegN=loadTable<1,int>(rootpath / "edgeSegN.txt")[0];
	params.segVesType=loadTable<1,int>(rootpath / "segVesType.txt")[0];
	params.RelVol=loadTable<2,float>(rootpath / "Vtime.txt")[1]; // was RelVol = Vtime(:,vol_iFrame) ./ Vtime(:,1); in matfile #94
	params.segDiam=loadTable<1,float>(rootpath / "segDiam.txt")[0];
	params.ROI_start=ROI_start;
	params.ROI_end=ROI_end;
	params.spc=spc;
	params.phi_angle=phi_angle;
	params.omega_angle=omega_angle;
	params.B0=B0;
}

/*******************
 * Here is where something starts to happen. 
 * doCalc constructs masks of vessels, Hct, SatO2 delta_B and others, from a format, 
 * where the vasculature is decomposed into line segments of varying diameter ("Edges") between "Nodes".
 * each node has a Hematocrit and pO2 value.
 ******************/
data::TypedChunk<bool> CalcMask::makeMasks(calctype type, data::TypedChunk<float> &dChimask,data::TypedChunk<float> &T2mask,float Vdil, util::ProgressFeedback *feedback)const
{
	int iE=0;
    const util::ivector3 shape = params.ROI_end-params.ROI_start;  // #redundant
	const size_t &nx=shape[0],&ny=shape[1],&nz=shape[2];
	data::MemChunk<bool> Vm(nx,ny,nz,1,true);
	
	
	std::vector<float> po2=loadTable<2,float>(input_root / "cg.txt")[type];
	
	/******************  Generate SatO2 and T2 volumes ********************/
	{
		data::MemChunk<float> Hctmask(nx,ny,nz);
		data::MemChunk<float> SatO2mask(nx,ny,nz); //was ini (ref)
		

        // fill tissue with constamnt T2 ('T2 constants' in matlab)
        for(int z=0; z<nz; z++)
            for(int y=0; y<ny; y++)
                for(int x=0; x<nx;x++){
                    T2mask.voxel<float>(x,y,z)=1000/(1.74*params.B0+7.77); //in msec (from Uludag 2009 formua (5))
//                     T2starmask.voxel<float>(x,y,z) = 1000/(3.74*params.B0+9.77);// in msec (from Uludag 2009 formula (6))
                }
// 

		if(feedback)
			feedback->show(nodeEdges.size(),"Drawing Vessels ...");

		for(edge e:nodeEdges){
			iE++;
			if(feedback)
				feedback->progress();
			const auto start_node = e.first; // i1
			const auto end_node = e.second; //note its not that we care for anything between start and end other than the "edge" i2
			size_t nSteps;
			std::vector<bool> nodeMask(nodePos_um.size(),true);  // mask for the path betwen the nodes

			if (nodeMask[start_node] || nodeMask[end_node]){ // why are both ends cheked?
				// node_mask könnte abgeschaft werden, wenn wir einfach start/end_node aus der liste entfernen
				fvector3 dxyz; 
				const auto foo = std::max(
					sqrt(params.segDiam[params.nodeSegN[start_node]]*params.RelVol[params.nodeSegN[start_node]]) ,
					sqrt(params.segDiam[params.nodeSegN[end_node]]*params.RelVol[params.nodeSegN[end_node]])
				)/2; // strange formula!! // wurzel kann nach außen
				const auto r = std::min(std::max<float>(1,foo*Vdil),maxR); // in um; maxR is never reached, but well, it doesnt hurt


				const fvector3 p1 = nodePos_um[start_node]; //start point in um voxels
				const fvector3 p2 = nodePos_um[end_node]; //start point in um voxels
				const float d12 = util::len(p1-p2);
				const float stepLen = std::max<float>(r*maskSphereSpacing,1); // maskSphereSpacing = 0.05um
				if(stepLen<d12){
					dxyz = (p2-p1)/d12*stepLen;// used as a differential
					nSteps = std::ceil(d12/stepLen);
				} else {
					dxyz = (p2-p1);
				}

				//@todo geht bestimmt cleverer
				for(int i=0;i<nodePos_um.size();i++){
					if(util::len(nodePos_um[i]-p1) < r*maskSphereSpacing)
						nodeMask[i]=false;
					if(util::len(nodePos_um[i]-p2) < r*maskSphereSpacing)
						nodeMask[i]=false;
				}

				int rr=std::ceil(r); //in voxels

				if(start_node==end_node)
					LOG(MCLog,warning) << "start_node and end_node are equal (" << start_node << ")";
				else
					LOG_IF(d12==0,MCLog,warning) << "d1 (nodePos_um.txt:" << start_node+1 << ") and d2 (nodePos_um.txt:" << end_node+1 << ") are equal (" << p1 << ")";

				for(fvector3 p=p1;util::len(p-p2)>stepLen;p+=dxyz){ // @todo noch checken + sqrt einsparen
                    // to plot the vessel on the mask, a ball of the vessel diameter is drawn along the vessel
					util::ivector3 pr{(int)std::round(p[0]),(int)std::round(p[1]),(int)std::round(p[2])};
					double SatO2_value =
						util::len(p-p1)/d12*so2_func(po2[end_node] , params.spc) +
						util::len(p-p2)/d12*so2_func(po2[start_node] , params.spc); //weighted average

					const float vesselHct = Hct[params.segVesType[params.edgeSegN[iE]]]; //mat-file #203
					// find voxels that lie in ball with radius r
					for(int zs=-rr; zs<=rr; zs++)
						for(int ys=-sqrt(rr*rr-zs*zs); ys<=sqrt(rr*rr-zs*zs); ys++)
							for(int xs=-sqrt(rr*rr-zs*zs-ys*ys); xs<=sqrt(rr*rr-zs*zs-ys*ys); xs++){
								assert(zs*zs+ys*ys+xs*xs<=rr*rr);
								size_t x= xs +pr[0];
								size_t y= ys +pr[1];
								size_t z= zs +pr[2];
                                //check whether/which voxels of ball lie in ROI
								if(
									x<params.ROI_start[0] || x>=params.ROI_end[0] ||
									y<params.ROI_start[1] || y>=params.ROI_end[1] ||
									z<params.ROI_start[2] || z>=params.ROI_end[2]
								){
									continue;
								} else {
									SatO2mask.voxel<float>(x,y,z) = SatO2_value;
									Hctmask.voxel<float>(x,y,z) = vesselHct;
									Vm.voxel<bool>(x,y,z) = true;
									//T2 volume (in msec) fomulas see uludag et al
									T2mask.voxel<float>(x,y,z)=1000/((2.74*params.B0-0.6)+(12.67*params.B0*params.B0*pow((1-SatO2_value),2))); // #345 (T2_TISSUE is same value #67)
// 									T2starmask.voxel<float>(x,y,z) = 1000/pow((100+500*SatO2_value),2); // 100 and 500 are relaxation rates taken from from Zhao 2007 and Silverstein 2003
									//dChi volume
									dChimask.voxel<float>(x,y,z) = D_CHI_0*vesselHct*(1-SatO2_value);
								}
							}
					if((--nSteps)<=0)
						break;
				}
			}
		}
	}

	return Vm;
}

data::TypedChunk<float> CalcMask::get_pert_B(const data::Chunk &reference,calctype type) const
{
	util::vector3<size_t> shape{reference.getDimSize(0),reference.getDimSize(1),reference.getDimSize(2)};
	data::MemChunk<float> pert_B(shape[0],shape[1],shape[2]);
	static_cast<util::PropertyMap&>(pert_B)=reference;
	auto Hvox=reference.getValueAs<util::fvector3>("voxelSize");

	const fvector3 origin = shape/2.f-fvector3{0.5,0.5,0.5}; //@todo won't work with discrete values (ist nur halb geometrisch)
	const float a=Hvox[0]/2;

	const fvector3  r0{
		float(std::cos(params.phi_angle*M_PI/180)*sin(params.omega_angle*M_PI/180)),
		float(std::sin(params.phi_angle*M_PI/180)*sin(params.omega_angle*M_PI/180)),
		float(std::cos(params.omega_angle*M_PI/180))
	}; //unit vector oriented along the B-field
	const float r0_norm=1;//norm(r0); %should be one

	for(int z=0; z<shape[2]; z++)
		for(int y=0; y<shape[1]; y++)
			for(int x=0; x<shape[0];x++){
				fvector3 r{(float)x,(float)y,(float)z};
				r-=origin;
				const float r_norm=util::len(r); //@todo teuer
				if(r_norm==0){
					pert_B.voxel<float>(x,y,z)=0;
				}else{
					float costheta=util::dot(r,r0)/(r_norm*r0_norm);
					pert_B.voxel<float>(x,y,z)=params.B0*2/M_PI*std::pow(a,3)/std::pow(r_norm,3)*(3*std::pow(costheta,2)-1);
				}
			}
	return pert_B;
}


data::TypedChunk< std::complex< float > > CalcMask::get_fft_pert_B(data::TypedChunk< float > dChimask,calctype type) const
{
	LOG(MCLog,notice) << "computing fft_pert_B" << (type==excited?" (excited)":" (base)");;
	auto pert_B = get_pert_B(dChimask,type);
	auto fft_succep_volume = math::fft_single(dChimask,false);
	auto fft_pert_B=math::fft_single(pert_B,false);
	
	std::transform(
		fft_pert_B.begin(),fft_pert_B.end(),fft_succep_volume.begin(),fft_pert_B.begin(),
		[](std::complex< float > a, std::complex< float > b){return a*b;} 
	);
	
	return fft_pert_B;
}

data::TypedChunk<float> CalcMask::get_delta_B(data::TypedChunk< float > dChimask,calctype type)const{
	
	/****************** Construct delta_B map *********************/
	//(we assume cube is repeated periodically in space, no padding in FFT)
	// #441
	auto fft_pert_B=get_fft_pert_B(dChimask,type);
	
	LOG(MCLog,notice) << "computing delta_B" << (type==excited?" (excited)":" (base)");
	data::MemChunk<float> delta_B = transform<float>( // #445
		math::fft_single(fft_pert_B,true),
		[](const std::complex< double > &v){return v.real();}
	);

	return delta_B;
}

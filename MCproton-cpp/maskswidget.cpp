/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2016  <copyright holder> <reimer@cbs.mpg.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "maskswidget.hpp"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QFileDialog>

#include <isis/adapter/qt5/common.hpp>
#include <isis/core/vector.hpp>
#include <isis/adapter/qt5/display.hpp>
#include <QLineEdit>

void tryDisplay(CalcThread *calc){
	if(calc->isFinished()){
		const calctype type=calc->type;
		isis::qt5::display(calc->delta_B,std::string("delta_B")+(type==excited ? " (excited)":""));
		isis::qt5::display(calc->T2mask,std::string("T2mask")+(type==excited ? " (excited)":""));
		isis::qt5::display(calc->T2starmask,std::string("T2*mask")+(type==excited ? " (excited)":""));
	}
		
}


MasksWidget::MasksWidget(calctype in_type, QLineEdit *in_storepath, QWidget* parent):QGroupBox(type==base ? "base":"excited"),type(in_type),storePath(in_storepath)
{
	QFrame *dChimask_frame=new QFrame(), *T2mask_frame=new QFrame(), *T2starmask_frame=new QFrame();
	setLayout(new QVBoxLayout());
	QPushButton *doMasksBtn = new QPushButton("compute masks");
	layout()->addWidget(doMasksBtn);
	
	for(QFrame *f:{dChimask_frame, T2mask_frame, T2starmask_frame}){
		f->setLayout(new QHBoxLayout());
		layout()->addWidget(f);
	}
	
	layout()->addWidget(feedback);

	connect(doMasksBtn,SIGNAL(clicked()),SLOT(makeMasks()));


	dChimask_frame->layout()->addWidget(new QLabel("dChimask"));
	QPushButton *load_btn=new QPushButton("load",this), *view_btn=new QPushButton("view",this);
	dChimask_frame->layout()->addWidget(load_btn);
	dChimask_frame->layout()->addWidget(view_btn);
}

void MasksWidget::makeMasks()
{
	QString param_dir=QFileDialog::getExistingDirectory(this,"Select path with parameter files");
	
	if(param_dir.isEmpty())
		return;
	
	isis::util::ivector3 ROI_start{0,0,0},ROI_end{128,128,128};// todo use actual ROI size here
	const CalcMask mask(
		param_dir.toStdString(),
		storePath->text().toStdString(), // store path for nifti
		ROI_start,ROI_end,
		mouse,
		90,
		45,
		{1.24,1.24,2}, //voxelsize
		3 //B0
	);
    const auto size=ROI_end-ROI_start;
	const float vdil=1; // todo wtf is this
    
	calc   =new CalcThread(this,mask,type,size,vdil,feedback);
	connect(calc,SIGNAL(finished()),SLOT(onMasksReady()));
	calc->start();
	
}
void MasksWidget::onMasksReady()
{
	delta_B=calc->delta_B;
	T2mask=calc->T2mask;
	T2starmask=calc->T2starmask;
}

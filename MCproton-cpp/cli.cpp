#include <iostream>
#include <cmath>
#include <isis/core/application.hpp>
#include <isis/core/progressfeedback.hpp>
#include <isis/core/chunk.hpp>
#include "calcmask.hpp"
#include "protonsim.hpp"
#include <fstream>

using namespace isis;

const size_t iFrame=2; //in the launcher called O2Index
const size_t vol_iFrame=2; //Flowindex
const size_t ref_iFrame=1;//O2baseIndex
const size_t vol_ref_iFrame=1; //FlowbaseIndex

const float D_CHI_0 = 4*M_PI*0.264e-6; // suceptibility of deoxygenated blood (from  Christen et al 2011)
const float GAMMA=2.675e5; // rad/Tesla/msec
const float Dcoeff=1;

int main(int argc, char **argv) {

	util::Application app("monte_carlo");
	util::DefaultMsgPrint::stopBelow(warning);

	app.parameters["B0"]=3.f;
	app.parameters["omega"]=45;
	app.parameters["nprotons"]=10000;
	app.parameters["dt"]=0.2;
	app.parameters["ROI_start"]=util::ivector3{0,0,0};
	app.parameters["ROI_end"]=util::ivector3{0,0,0};
	app.parameters["voxelsize"]=util::fvector3{1.24,1.24,2}; //for resampling

	app.parameters["Gx"]=50; // Gradient for readout in mT/m
	app.parameters["TE"]=30; //Echo time (ms)

	app.parameters["out"]=std::string();
	app.parameters["out"].setDescription("Output text file");
	
	app.parameters["display"]=false;
	app.parameters["display"].needed()=false;
	app.parameters["display"].setDescription("Display interim data");

	app.parameters["store_path"]=std::string("");
	app.parameters["store_path"].needed()=false;
	app.parameters["store_path"].setDescription("Store interim data in given directory");

	app.parameters["B0"].needed()=false;
	app.parameters["omega"].needed()=false;
	app.parameters["nprotons"].needed()=false;
	app.parameters["dt"].needed()=false;

	app.parameters["ROI_start"].needed()=false;
	app.parameters["ROI_end"].needed()=false;
	app.parameters["voxelsize"].needed()=false;
	app.parameters["Gx"].needed()=false;
	app.parameters["TE"].needed()=false;
	app.parameters["Dcoeff"].needed()=false;



	app.parameters["root_path"]=std::string(".");
	app.parameters["root_path"].setDescription("Path to load the parameter sets from");
	app.parameters["root_path"].needed()=false;;
	
	app.parameters["vdil"]=1.f;
	app.parameters["vdil"].setDescription("Vascular dilatation for all vessels");
	app.parameters["vdil"].needed()=false;

	app.addLogging<MCLog>("");
	app.addLogging<MCDebug>("");

	app.init(argc,argv);

	std::unique_ptr<util::ConsoleFeedback> feedback(new util::ConsoleFeedback);

	//T2 constants
	const auto T2_TISSUE=1000/(1.74*app.parameters["B0"].as<float>()+7.77); //in msec (from Uludag 2009 formua (5)) #.
	const auto T2star_TISSUE=1000/(3.74*app.parameters["B0"].as<float>()+9.77); //in msec (from Uludag 2009 formula (6)) #.

	util::ivector3 ROI_end = 
		app.parameters["ROI_end"].isParsed() ?
			app.parameters["ROI_end"].as<util::ivector3>() :
			util::ivector3{512,512,512}
	;


	CalcMask mask(
		app.parameters["root_path"].as<std::string>(),
		app.parameters["store_path"].as<std::string>(),
		app.parameters["ROI_start"],
		ROI_end,
		mouse,
		90,
		45,
		app.parameters["voxelsize"],
		app.parameters["B0"].as<float>()
	);
    const auto size=ROI_end-app.parameters["ROI_start"].as<util::ivector3>();// todo use actual ROI size here
    
	data::MemChunk<float> dChimask = data::MemChunk<float>(size[0],size[1],size[2]);
	data::MemChunk<float> dChimask_base = data::MemChunk<float>(size[0],size[1],size[2]);
	data::MemChunk<float> T2mask = data::MemChunk<float>(size[0],size[1],size[2]);
	data::MemChunk<float> T2starmask = data::MemChunk<float>(size[0],size[1],size[2]);
	data::MemChunk<float> T2mask_base = data::MemChunk<float>(size[0],size[1],size[2]);
	data::MemChunk<float> T2starmask_base =data::MemChunk<float>(size[0],size[1],size[2]);

	        mask.makeMasks(excited,     dChimask,     T2mask,     T2starmask,app.parameters["vdil"],feedback.get());
	auto Vm=mask.makeMasks(   base,dChimask_base,T2mask_base,T2starmask_base,                     1,feedback.get());
	
	const auto delta_B = mask.get_delta_B(dChimask,excited);
	const auto delta_B_base = mask.get_delta_B(dChimask_base,base);

	
	ProtonSim::return_val erg,erg_IV,erg_EV;
	ProtonSim sim(Vm,delta_B,delta_B_base,T2mask,T2mask_base,app.parameters["voxelsize"]);
	std::tie(erg,erg_IV,erg_EV) =sim.simulate(app.parameters["nprotons"]);
	
	LOG(MCLog,notice) << "writing signal data to " << app.parameters["out"].as<std::string>();
	std::ofstream ofile(app.parameters["out"].as<std::string>());
	ofile << "signal_GE_base\tsignal_GE_exci\tsignal_SE_base\tsignal_SE_exci" << '\t';
	ofile << "signal_GE_base_IV\tsignal_GE_exci_IV\tsignal_SE_base_IV\tsignal_SE_exci_IV" << '\t';
	ofile << "signal_GE_base_EV\tsignal_GE_exci_EV\tsignal_SE_base_EV\tsignal_SE_exci_EV" ;
	ofile << std::endl;
	ofile << std::fixed << std::setprecision(8);
	assert(erg.signal_GE_base.size()==erg.signal_SE_base.size());
	for(int i=0;i<erg.signal_GE_base.size();i++){
		ofile << erg.signal_GE_base[i].real() << '\t' << erg.signal_GE_exci[i].real() << '\t' << erg.signal_SE_base[i].real() << '\t' << erg.signal_SE_exci[i].real() << '\t';
		ofile << erg_IV.signal_GE_base[i].real() << '\t' << erg_IV.signal_GE_exci[i].real() << '\t' << erg_IV.signal_SE_base[i].real() << '\t' << erg_IV.signal_SE_exci[i].real() << '\t';
		ofile << erg_EV.signal_GE_base[i].real() << '\t' << erg_EV.signal_GE_exci[i].real() << '\t' << erg_EV.signal_SE_base[i].real() << '\t' << erg_EV.signal_SE_exci[i].real();
		ofile << std::endl;
	}
    return 0;
}




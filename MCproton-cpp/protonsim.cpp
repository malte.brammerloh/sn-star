/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2016  <copyright holder> <reimer@cbs.mpg.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "protonsim.hpp"
#include <random>

using namespace isis;

ProtonSim::ProtonSim(
	const TypedChunk<bool> in_Vm, 
	const TypedChunk<float> in_delta_B, 
	const TypedChunk<float> in_delta_B_base, 
	const TypedChunk<float> in_T2mask, 
	const TypedChunk<float> in_T2mask_base, 
	std::array<float,3> in_Hvox, 
	double in_dt,
	double in_TE,
	double in_Dcoeff,
	isis::util::ProgressFeedback *feedback
):
delta_B(in_delta_B),delta_B_base(in_delta_B_base),
T2mask(in_T2mask),T2mask_base(in_T2mask_base),
Vm(in_Vm),
shape(in_Vm.getSizeAsVector()),Hvox(in_Hvox), //todo check voxelsize
dt(in_dt),
TE(in_TE),
Dcoeff(in_Dcoeff),
progress(feedback)
{}

std::tuple<ProtonSim::return_val, ProtonSim::return_val, ProtonSim::return_val> ProtonSim::simulate(int nprotons)
{
	const size_t &nx=shape[0],&ny=shape[1],&nz=shape[2];    	 	// #todo, we often still assume that roi starts with 0 when iterating
	const float Gx=50; // Gradient for readout in mT/m
	const float GX_um=Gx*1e-6*1e-3; // Now in T/um
	const float GAMMA=2.675e5; // rad/Tesla/msec
	//const float Dcoeff =1/2.56; // Proton (water) diffusion coefficient(um2/msec) / voxelscale
	//std::cout << "Resolution of 2.56 µm per pixel assumed." << std::endl;
	const float Dvox=Dcoeff/(Hvox[0]*Hvox[0]); //squared vector length seems better, but thats what matlab says
	const float sigma=sqrt(2*Dvox*dt); //square of length seem better, but thats what the matlab says

	std::cout<< sigma << std::endl;	

	const int ntime_step = (int)TE/dt;

	return_val ret = {
		std::vector<std::complex<float>>(ntime_step,0),
		std::vector<std::complex<float>>(ntime_step,0),
		std::vector<std::complex<float>>(ntime_step,0),
		std::vector<std::complex<float>>(ntime_step,0)
	};
	return_val ret_IV=ret,ret_EV=ret;
	
// baseline
	std::vector<float> protons_B_base(nprotons,0);
	std::vector<float> protons_T2_base(nprotons,0);
	std::vector<float> protons_T2star_base(nprotons,0);
	std::vector<std::complex<float>> protons_phase_GE_base(nprotons,0);		//phase_GE_base before but has size nprotons
	std::vector<std::complex<float>> protons_phase_SE_base(nprotons,0); //phase_SE_base before but has size nprotons
	
// excited
	std::vector<float> protons_B_exci(nprotons,0);
	std::vector<float> protons_T2_exci(nprotons,0);
// 	std::vector<float> protons_T2star_exci(nprotons,0);
	std::vector<std::complex<float>> protons_phase_GE_exci(nprotons,0); 		//phase_GE before but has size nprotons
	std::vector<std::complex<float>> protons_phase_SE_exci(nprotons,0);//phase_SE before but has size nprotons
	
// independent
	std::vector<float> protons_gradient_GE(nprotons,0);
	std::vector<float> protons_gradient_SE(nprotons,0);

//	std::vector< util::fvector3 > proton_pos_(nprotons*ntime_step); // output all proton positions for generation of heat map.
	
	std::vector< util::fvector3 > proton_pos(nprotons); // proton_pos is float even though masks are not continous. this is important for random walk.
    
	/////// intitialize proton_pos ///////
	
	std::random_device rd;
	std::mt19937 gen(rd());
	// homogeneously distributed random mumber generator for position initialization 
	std::uniform_int_distribution<> dis_x(0, shape[0]-1),dis_y(0, shape[1]-1),dis_z(0, shape[2]-1);
	// normally distributed random number genarator for proton movement
	std::normal_distribution<float> dist_n(0, sigma);

	std::vector<bool> IV_proton_Vm(nprotons); // originally proton_Vm
	std::vector<bool> IV_proton_Vm_new(nprotons);
	util::fvector3 proton_pos_new; // originally rnum 


	// Generate initial position for protons inside the ROI
	for (int i=0;i<nprotons;i++){
		proton_pos[i][0]= dis_x(gen);
		proton_pos[i][1]= dis_y(gen);
		proton_pos[i][2]= dis_z(gen); 
		//identify extravascular proton
		IV_proton_Vm[i] = Vm.voxel<bool>( proton_pos[i][0], proton_pos[i][1], proton_pos[i][2] );
	}
	
	//Spin Echo params
	const int phase_shift_index=std::round(TE/(2*dt)); // half_echo_index
	const int echo_index=std::min<int>((TE/dt),ntime_step);
	const int start1_se_index=std::round(TE/(4*dt)-TE/(8*dt));
	const int stop1_se_index =std::round(TE/(4*dt)+TE/(8*dt));
	const int start2_se_index=std::round(TE/dt    -TE/(4*dt));
	const int stop2_se_index =std::round(TE/dt    +TE/(4*dt));
	 


	//Gradient Echo params
	const int start_ge_index=std::round(TE/(3*dt)); 	
	const int flip_ge_index=std::round(2*TE/(3*dt));
	const int stop_ge_index=std::round(4*TE/(3*dt));

	//Gradient init
	int8_t ge_gradient_weight=0; 
	int8_t se_gradient_weight=0; 
	
	// start random walk
	if(progress)
		progress->show(ntime_step,std::string("simulating ") + std::to_string(ntime_step) + " time steps" );
	
	// For heat map
 	std::ofstream posfile("traj.txt");
 	posfile << std::fixed << std::setprecision(12);
 	posfile << "# x\t y\t z\t PhaseGE real\t PhaseGE imag\t PhaseSEreal\t PhaseSEimag\tdelta B\n";
	
	for (int tStep=0; tStep < ntime_step; tStep++){
		if(progress)progress->progress();
		//Gradient in readout for GE
		//if(start_ge_index==tStep)
		//	ge_gradient_weight=1;
		//else if (flip_ge_index==tStep)
		//	ge_gradient_weight=-1;
		//else if (stop_ge_index==tStep)
		//	ge_gradient_weight=0;
	
		//Gradient in readout for SE // Theoretically more convenient: No redout gradients. 
		//if(start1_se_index==tStep)
		//	se_gradient_weight=1;
		//else if (stop1_se_index==tStep)
		//	se_gradient_weight=0;
		//else if (start2_se_index==tStep)
		//	se_gradient_weight=1;
		//else if (stop2_se_index==tStep)
		//	se_gradient_weight=0;
		
		size_t IV_count=0;
		for (int nr_proton=0; nr_proton<nprotons;nr_proton++){
			//move proton temporarily
			proton_pos_new = proton_pos[nr_proton]+ util::fvector3{float(dist_n(gen)),float(dist_n(gen)),float(dist_n(gen))};
			if( proton_pos_new[0] <  0 || proton_pos_new[0] >= shape[0]   || 
				proton_pos_new[1] <  0 || proton_pos_new[1] >= shape[1]   || 
				proton_pos_new[2] <  0 || proton_pos_new[2] >= shape[2]  
			){
				continue;
			} else {
				IV_proton_Vm_new[nr_proton] = Vm.voxel<bool>(proton_pos_new[0],proton_pos_new[0],proton_pos_new[0]);
            }
			//find the ones who crossed vessel boundary (try 100 times otherwise let it go cross the boundary)
			for (int crossing_count=0;IV_proton_Vm[nr_proton] != IV_proton_Vm_new[nr_proton] && crossing_count<100;crossing_count++){ 
				proton_pos_new = proton_pos[nr_proton] + util::fvector3{float(dist_n(gen)),float(dist_n(gen)),float(dist_n(gen))};
				//check if proton went outside ROI
				if( proton_pos_new[0] <  0 || proton_pos_new[0] >= shape[0]   || 
					proton_pos_new[1] <  0 || proton_pos_new[1] >= shape[1]   || 
					proton_pos_new[2] <  0 || proton_pos_new[2] >= shape[2]  
				){
					proton_pos_new = proton_pos[nr_proton]; // if out of ROI do nothing
				}
				IV_proton_Vm_new[nr_proton] = Vm.voxel<bool>( proton_pos_new[0],proton_pos_new[1],proton_pos_new[2] );// rounding done here??
			}
			proton_pos[nr_proton] = proton_pos_new; // move proton accordingly
			IV_proton_Vm[nr_proton] = IV_proton_Vm_new[nr_proton]; //  update IV list
            
			//  proton states
			util::vector4<size_t> proton_pos_i;
			for(int i=0;i<3;i++)
				proton_pos_i[i]=proton_pos[nr_proton][i];
			proton_pos_i[3]=0;
			
			protons_B_exci[nr_proton] = delta_B.voxel<float>( proton_pos_i);
			protons_B_base[nr_proton] = delta_B_base.voxel<float>( proton_pos_i); // #todo delta_B_base has no value yet
			protons_T2_exci[nr_proton] = T2mask.voxel<float>( proton_pos_i);
			protons_T2_base[nr_proton] = T2mask_base.voxel<float>( proton_pos_i); //#todo base 
// 			protons_T2star_exci[i] = T2starmask.voxel<float>( proton_pos_i);
// 			protons_T2star_base[i] = T2starmask_base.voxel<float>( proton_pos_i); //#todo base
			protons_gradient_GE[nr_proton] = ge_gradient_weight*(proton_pos[nr_proton][0]-nx/2)*GX_um;
			protons_gradient_SE[nr_proton] = se_gradient_weight*(proton_pos[nr_proton][0]-nx/2)*GX_um;
			
			if(IV_proton_Vm[nr_proton]==0){
				protons_phase_GE_base[nr_proton]+=std::complex<float>(0,GAMMA*(protons_B_base[nr_proton]+protons_gradient_GE[nr_proton])*dt);
				protons_phase_GE_exci[nr_proton]+=std::complex<float>(0,GAMMA*(protons_B_exci[nr_proton]+protons_gradient_GE[nr_proton])*dt); 
				protons_phase_SE_base[nr_proton]+=std::complex<float>(0,GAMMA*(protons_B_base[nr_proton]+protons_gradient_SE[nr_proton])*dt); 
				protons_phase_SE_exci[nr_proton]+=std::complex<float>(0,GAMMA*(protons_B_exci[nr_proton]+protons_gradient_SE[nr_proton])*dt); 
			}

// 			protons_phase_GE_base[nr_proton]+=-(1./protons_T2_base[nr_proton])*dt;
// 			protons_phase_GE_exci[nr_proton]+=-(1./protons_T2_exci[nr_proton])*dt; 
// 			protons_phase_SE_base[nr_proton]+=-(1./protons_T2_base[nr_proton])*dt; 
// 			protons_phase_SE_exci[nr_proton]+=-(1./protons_T2_exci[nr_proton])*dt; 

			// spin echo 180 degrees flip
			if (tStep == phase_shift_index){
				protons_phase_SE_base[nr_proton]=std::conj(protons_phase_SE_base[nr_proton]); 
				protons_phase_SE_exci[nr_proton]=std::conj(protons_phase_SE_exci[nr_proton]);
			}
			
			 //  signal at time tStep
			ret.signal_GE_base[tStep] += exp(protons_phase_GE_base[nr_proton]); //#todo is signal_GE_base initialized to 0??
			ret.signal_GE_exci[tStep] += exp(protons_phase_GE_exci[nr_proton]);
			ret.signal_SE_base[tStep] += exp(protons_phase_SE_base[nr_proton]);
			ret.signal_SE_exci[tStep] += exp(protons_phase_SE_exci[nr_proton]);
			
			if(IV_proton_Vm[nr_proton]){ // IV signal at time tStep
				IV_count++;
				ret_IV.signal_GE_base[tStep] += exp(protons_phase_GE_base[nr_proton]); //#todo is signal_GE_base initialized to 0??
				ret_IV.signal_GE_exci[tStep] += exp(protons_phase_GE_exci[nr_proton]);
				ret_IV.signal_SE_base[tStep] += exp(protons_phase_SE_base[nr_proton]);
				ret_IV.signal_SE_exci[tStep] += exp(protons_phase_SE_exci[nr_proton]);
			} else {  // EV signal at time tStep
				ret_EV.signal_GE_base[tStep] += exp(protons_phase_GE_base[nr_proton]); //#todo is signal_GE_base initialized to 0??
				ret_EV.signal_GE_exci[tStep] += exp(protons_phase_GE_exci[nr_proton]);
				ret_EV.signal_SE_base[tStep] += exp(protons_phase_SE_base[nr_proton]);
				ret_EV.signal_SE_exci[tStep] += exp(protons_phase_SE_exci[nr_proton]);
			}				
			// todo differentiantion between extravascular and intravascular, if needed
		}
		
		//////////////////////////////////////////// For autocorrelation function and magnetization map:
 		for(int i=0;i<nprotons;i++){
 			posfile << proton_pos[i][0] << '\t'<< proton_pos[i][1] << '\t'<< proton_pos[i][2]  \
 				<< '\t' << std::real(protons_phase_GE_base[i]) << '\t' << std::imag(protons_phase_GE_base[i]) \
 				<< '\t' << std::real(protons_phase_SE_base[i]) << '\t' << std::imag(protons_phase_SE_base[i]) \
 				<< '\t' << protons_B_exci[i] << '\t';
 			posfile << std::endl;
 		}
		
		ret.signal_GE_base[tStep] = std::abs(ret.signal_GE_base[tStep])/nprotons;
		ret.signal_GE_exci[tStep] = std::abs(ret.signal_GE_exci[tStep])/nprotons;
		ret.signal_SE_base[tStep] = std::abs(ret.signal_SE_base[tStep])/nprotons;
		ret.signal_SE_exci[tStep] = std::abs(ret.signal_SE_exci[tStep])/nprotons;
		
		ret_IV.signal_GE_base[tStep] = std::abs(ret_IV.signal_GE_base[tStep])/IV_count;
		ret_IV.signal_GE_exci[tStep] = std::abs(ret_IV.signal_GE_exci[tStep])/IV_count;
		ret_IV.signal_SE_base[tStep] = std::abs(ret_IV.signal_SE_base[tStep])/IV_count;
		ret_IV.signal_SE_exci[tStep] = std::abs(ret_IV.signal_SE_exci[tStep])/IV_count;
		
		ret_EV.signal_GE_base[tStep] = std::abs(ret_EV.signal_GE_base[tStep])/(nprotons-IV_count);
		ret_EV.signal_GE_exci[tStep] = std::abs(ret_EV.signal_GE_exci[tStep])/(nprotons-IV_count);
		ret_EV.signal_SE_base[tStep] = std::abs(ret_EV.signal_SE_base[tStep])/(nprotons-IV_count);
		ret_EV.signal_SE_exci[tStep] = std::abs(ret_EV.signal_SE_exci[tStep])/(nprotons-IV_count);
	}
	return std::make_tuple(ret,ret_IV,ret_EV);
}

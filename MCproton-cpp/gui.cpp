#include <iostream>
#include <isis/adapter/qt5/common.hpp>
#include <isis/adapter/qt5/qtapplication.hpp>
#include <isis/adapter/qt5/display.hpp>
#include "ctrlwidget.hpp"

using namespace isis::qt5;

int main(int argc, char **argv) {

	QtApplication app("MCprotonGUI");
	app.init(argc,argv);
	
	CtrlWidget ctrl("MCprotonGUI");
	ctrl.show();

    return app.exec();
}



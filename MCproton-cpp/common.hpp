#ifndef COMMON_H
#define COMMON_H

using namespace isis;

struct MCLog {static const char *name() {return "McProton";}; enum {use = _ENABLE_LOG};};
struct MCDebug {static const char *name() {return "McProtonDebug";}; enum {use = _ENABLE_DEBUG};};

#endif //COMMON_H

#include <isis/core/application.hpp>
#include <isis/core/io_factory.hpp>
#include <isis/core/progressfeedback.hpp>
#include "common.hpp"
#include "calcmask.hpp"

int main(int argc, char **argv) {

	util::Application app("vascular mask generator");

	(app.parameters["ROI_start"]=util::ivector3{0,0,0}).needed()=false;;
	(app.parameters["ROI_end"]=util::ivector3{0,0,0}).needed()=false;;
	
	(app.parameters["B0"]=3.f).needed()=false;
	(app.parameters["omega"]=45.f).needed()=false;

	app.parameters["param_path"]=std::string("");
	app.parameters["param_path"].setDescription("Path to load the parameter sets from");

	(app.parameters["vdil"]=1.f).needed()=false;;
	app.parameters["vdil"].setDescription("Vascular dilatation for all vessels");

	app.parameters["voxelsize"]=util::fvector3{1.24,1.24,2}; //for resampling
	app.parameters["voxelsize"].needed()=false;

	app.addLogging<MCLog>("");
	app.addLogging<MCDebug>("");

	app.init(argc,argv);
	
	std::unique_ptr<util::ConsoleFeedback> feedback(new util::ConsoleFeedback);

	util::ivector3 ROI_end = 
		app.parameters["ROI_end"].isParsed() ?
			app.parameters["ROI_end"].as<util::ivector3>() :
			util::ivector3{512,512,512}
	;


	CalcMask mask(
		app.parameters["param_path"].as<std::string>(),
		app.parameters["ROI_start"],
		ROI_end,
		mouse,
		90,
		app.parameters["omega"],
		app.parameters["B0"]
	);
    const auto size=ROI_end-app.parameters["ROI_start"].as<util::ivector3>();// todo use actual ROI size here
    
	data::MemChunk<float> dChimask = data::MemChunk<float>(size[0],size[1],size[2],1,true);
	data::MemChunk<float> dChimask_base = data::MemChunk<float>(size[0],size[1],size[2],1,true);
	data::MemChunk<float> T2 = data::MemChunk<float>(size[0],size[1],size[2],1,true);
// 	data::MemChunk<float> T2starmask = data::MemChunk<float>(size[0],size[1],size[2],1,true);
	data::MemChunk<float> T2_base = data::MemChunk<float>(size[0],size[1],size[2],1,true);
// 	data::MemChunk<float> T2starmask_base =data::MemChunk<float>(size[0],size[1],size[2],1,true);
	
	auto Vm=mask.makeMasks(   base,dChimask_base,T2_base,                     1,feedback.get());
	        mask.makeMasks(excited,     dChimask,     T2,app.parameters["vdil"],feedback.get());
			
	Vm.setValueAs<util::fvector3>("voxelsize",app.parameters["voxelsize"]);
	T2.setValueAs<util::fvector3>("voxelsize",app.parameters["voxelsize"]);
	T2_base.setValueAs<util::fvector3>("voxelsize",app.parameters["voxelsize"]);
	dChimask.setValueAs<util::fvector3>("voxelsize",app.parameters["voxelsize"]);
	dChimask_base.setValueAs<util::fvector3>("voxelsize",app.parameters["voxelsize"]);

	data::IOFactory::write(mask.get_delta_B(dChimask_base,base), "delta_B.nii");
	data::IOFactory::write(mask.get_delta_B(dChimask,excited), "delta_B_base.nii");

	data::IOFactory::write(T2, "T2.nii");
	data::IOFactory::write(T2_base, "T2_base.nii");
	data::IOFactory::write(Vm, "Vm.nii");
	
}

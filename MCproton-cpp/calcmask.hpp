/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2016  Enrico Reimer <reimer@cbs.mpg.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef CALCMASK_H
#define CALCMASK_H
#include <vector>
#include <isis/core/vector.hpp>
#include <isis/core/valuearray.hpp>
#include <isis/core/chunk.hpp>
#include <isis/core/progressfeedback.hpp>

struct MCLog {static const char *name() {return "McProton";}; enum {use = _ENABLE_LOG};};
struct MCDebug {static const char *name() {return "McProtonDebug";}; enum {use = _ENABLE_DEBUG};};

using namespace isis;

enum species{human,mouse,rat};
enum calctype{base=0,excited=1};

//Hematocrit
static const float Hct[] = {
	.44, // arteries (from Griffeth et al 2011)
	.33,
	.44 
};


class CalcMask
{
public:
	typedef std::pair<size_t,size_t> edge;
	boost::filesystem::path input_root,store_path;
	struct parameters{
		species spc;
		util::ivector3 ROI_start,ROI_end;
		std::vector<float> segDiam;
		std::vector<int> edgeSegN;
		std::vector<int> nodeSegN;
		std::vector<int> segVesType; //@todo sollte max(edgeSegN) sein
		std::vector<float> RelVol;
		float phi_angle,omega_angle;
		int nprotons;
        std::array<float,3> Hvox;
		int B0; // if changed intravascular relaxation rate has to be adapted
// 		parameters(size_t nNodes,util::vector3<size_t> img_size);
	}params;
private:
	const float maskSphereSpacing = .05; // in um
	const float maxR = 90; //in um
	
	std::vector< util::fvector3 > nodePos_um; //round(im2.nodePos_um);
	size_t nNodes;
	const float D_CHI_0 = 4*3.1415*0.264e-6; // suceptibility of deoxygenated blood (from  Christen et al 2011)   

	std::vector<edge> nodeEdges;
	std::vector<edge> loadEdges(boost::filesystem::path fname);
	std::vector< util::fvector3 > loadVectors(boost::filesystem::path fname);

public:

	CalcMask(
		boost::filesystem::path rootpath, 
		util::ivector3 ROI_start, 
		util::ivector3 ROI_end, 
		species spc, 
		float phi_angle, 
		float omega_angle, 
		float B0
	);
	
	data::TypedChunk<bool> makeMasks(calctype type, data::TypedChunk<float> &dChimask,data::TypedChunk<float> &T2mask,float Vdil=1, util::ProgressFeedback *feedback=nullptr)const;
	data::TypedChunk< std::complex< float > >  get_fft_pert_B(data::TypedChunk< float> dChimask,calctype type)const;
	isis::data::TypedChunk< float > get_delta_B(isis::data::TypedChunk< float > dChimask, calctype type)const;
	data::TypedChunk<float> get_pert_B(const data::Chunk &reference,calctype type)const;
};

#endif // CALCMASK_H

/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2016  <copyright holder> <reimer@cbs.mpg.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef PROTONSIM_H
#define PROTONSIM_H

#include <isis/core/chunk.hpp>
#include <isis/core/progressfeedback.hpp>
#include <tuple>
#include <fstream>

using isis::data::TypedChunk;

class ProtonSim
{
	const TypedChunk<float> delta_B, delta_B_base, T2mask, T2mask_base, T2starmask, T2starmask_base;
	const TypedChunk<bool>  Vm;
	const std::array<size_t,4> shape;
	const std::array<float,3> Hvox;
	isis::util::ProgressFeedback *progress;

	const float dt = 0.2;
	const int TE = 30;
	double Dcoeff;


public:
	struct return_val{std::vector<std::complex<float>> signal_GE_base, signal_SE_base, signal_GE_exci, signal_SE_exci;};

	ProtonSim(
		const TypedChunk<bool>  in_Vm, 
		const TypedChunk<float> in_delta_B,
		const TypedChunk<float> in_delta_B_base,
		const TypedChunk<float> in_T2mask,
		const TypedChunk<float> in_T2mask_base,
		std::array<float,3> in_Hvox,
		double in_dt=.1,
		double in_TE=30,
		double in_Dcoeff=1,
		isis::util::ProgressFeedback *feedback=nullptr
	);
	std::tuple<return_val,return_val,return_val> simulate(int nprotons);
};

#endif // PROTONSIM_H

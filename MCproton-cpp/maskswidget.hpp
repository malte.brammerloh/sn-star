/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2016  <copyright holder> <reimer@cbs.mpg.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef MASKSWIDGET_H
#define MASKSWIDGET_H

#include <QGroupBox>
#include <QThread>

#include <isis/adapter/qt5/guiprogressfeedback.hpp>

#include "calcmask.hpp"

class QLineEdit;

class CalcThread : public QThread
{
	const CalcMask mask;
	float Vdil;
	util::ProgressFeedback *feedback;
public:
	calctype type;
	data::MemChunk<float> delta_B, T2mask, T2starmask;
	std::unique_ptr<isis::data::TypedChunk<bool>> VmPtr;
	
	CalcThread(QObject *parent,const CalcMask &in_mask,calctype in_type, isis::util::ivector3 size,float in_Vdil, util::ProgressFeedback *in_feedback):
	QThread(parent),mask(in_mask),
	delta_B(size[0],size[1],size[2]),
	T2mask(size[0],size[1],size[2]),
	T2starmask(size[0],size[1],size[2]),
	Vdil(in_Vdil),
	feedback(in_feedback),
	type(in_type){}
protected:
	void run()override{
		if(type==base)
			VmPtr.reset(new isis::data::TypedChunk<bool>(mask.makeMasks(base,delta_B,T2mask,T2starmask,Vdil,feedback)));
		else
			mask.makeMasks(excited,delta_B,T2mask,T2starmask,Vdil,feedback);
	}
};



class MasksWidget : public QGroupBox
{
    Q_OBJECT
    calctype type;
	CalcThread *calc;
	isis::qt5::GUIProgressFeedback *feedback=new isis::qt5::GUIProgressFeedback;
	QLineEdit *storePath;
	data::TypedChunk<float> delta_B, T2mask, T2starmask;

public:
	MasksWidget(calctype in_type,QLineEdit *in_storepath, QWidget* parent=nullptr);
private slots:
	void makeMasks();
	void onMasksReady();
};

#endif // MASKSWIDGET_H

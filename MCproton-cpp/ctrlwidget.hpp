/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2016  <copyright holder> <reimer@cbs.mpg.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef CTRLWIDGET_H
#define CTRLWIDGET_H

#include <QWidget>
#include <isis/adapter/qt5/guiprogressfeedback.hpp>
#include <isis/core/chunk.hpp>
#include <QThread>
#include "calcmask.hpp"

class QPushButton;
class QLineEdit;

class CtrlWidget : public QWidget
{
    Q_OBJECT
	QPushButton *doSimBtn;
	QLineEdit *storePath;

public:
    CtrlWidget(QString name);
private slots:
	void doSim();
signals:
	void masksReady();
};

#endif // CTRLWIDGET_H

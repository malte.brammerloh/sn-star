#include <iostream>
#include <cmath>
#include <isis/core/io_application.hpp>
#include <isis/core/progressfeedback.hpp>
#include <isis/core/chunk.hpp>
#include <isis/core/io_factory.hpp>
#include "calcmask.hpp"
#include "protonsim.hpp"
#include <fstream>

using namespace isis;

const size_t iFrame=2; //in the launcher called O2Index
const size_t vol_iFrame=2; //Flowindex
const size_t ref_iFrame=1;//O2baseIndex
const size_t vol_ref_iFrame=1; //FlowbaseIndex

const float D_CHI_0 = 4*M_PI*0.264e-6; // suceptibility of deoxygenated blood (from  Christen et al 2011)
const float GAMMA=2.675e5; // rad/Tesla/msec
const float Dcoeff=1;

int main(int argc, char **argv) {

	data::IOApplication app("monte_carlo",true,false);
	util::DefaultMsgPrint::stopBelow(warning);

	app.parameters["B0"]=7.f;
	app.parameters["omega"]=45;
	app.parameters["nprotons"]=10000;
	app.parameters["dt"]=0.1;
	app.parameters["Dcoeff"] =1/2.56;

	app.parameters["Gx"]=80; // Gradient for readout in mT/m
	app.parameters["TE"]=10.0; //Echo time (ms)

	app.parameters["store_path"]=std::string("");
	app.parameters["store_path"].needed()=false;
	app.parameters["store_path"].setDescription("Store interim data in given directory");

	app.parameters["B0"].needed()=false;
	app.parameters["omega"].needed()=false;
	app.parameters["nprotons"].needed()=false;
	app.parameters["dt"].needed()=false;

	app.parameters["Gx"].needed()=false;
	app.parameters["TE"].needed()=false;
	app.parameters["Dcoeff"].needed()=false;

	app.parameters["out"]=std::string();
	app.parameters["out"].setDescription("Output text file");

	app.parameters["vdil"]=1.f;
	app.parameters["vdil"].setDescription("Vascular dilatation for all vessels");
	app.parameters["vdil"].needed()=false;

	app.addLogging<MCLog>("");
	app.addLogging<MCDebug>("");

	app.init(argc,argv);

	std::unique_ptr<util::ConsoleFeedback> feedback(new util::ConsoleFeedback);

	//T2 constants
	// WARNING: T2 IS REMOVED FROM THE SIMULATION!!! JUST MESOSCOPICS HERE!
	const auto T2_TISSUE= 10000000;//100; //1000/(1.74*app.parameters["B0"].as<float>()+7.77); //in msec (from Uludag 2009 formua (5)) #.
	
	data::TypedImage<float> delta_B = app.fetchImage();
	const auto size=delta_B.getSizeAsVector();
	data::MemChunk<bool> Vm(size[0],size[1],size[2]);
	
	data::MemChunk<float> T2mask(size[0],size[1],size[2]), T2starmask(size[0],size[1],size[2]);
	
// 	data::IOFactory::write(delta_B,app.parameters["store_path"].as<std::string>()+"/dbg_delta_B.nii");
	
	for(float &f:T2mask)
		f=T2_TISSUE;
	
	
	//for(float &v:delta_B_f)
	//	v=v+1;
	
	//for(int x=0;x<size[0];x++)
	//	delta_B_f.voxel<float>(x,0,0);
	
	ProtonSim::return_val erg,erg_IV,erg_EV;
	ProtonSim sim(Vm,delta_B.getChunkAt(0),delta_B.getChunkAt(0),T2mask,T2mask,delta_B.getValueAs<util::fvector3>("voxelSize"),app.parameters["dt"],app.parameters["TE"],app.parameters["Dcoeff"],feedback.get());
	std::tie(erg,erg_IV,erg_EV) =sim.simulate(app.parameters["nprotons"]);
	
	LOG(MCLog,notice) << "writing signal data to " << app.parameters["out"].as<std::string>();
	std::ofstream ofile(app.parameters["out"].as<std::string>());

	ofile << "#simulation parameters: N = "<< app.parameters["nprotons"] << "Dcoeff = " << app.parameters["Dcoeff"];
	ofile << std::endl;

	ofile << "#time\tsignal_GE_base\tsignal_SE_base" << '\t';
	ofile << std::endl;
	ofile << std::fixed << std::setprecision(8);
	
	assert(erg.signal_GE_base.size()==erg.signal_SE_base.size());
	const double deltat = app.parameters["dt"];
	for(int i=0;i<erg.signal_GE_base.size();i++){
		ofile << deltat * i << '\t' << erg.signal_GE_base[i].real() << '\t' << erg.signal_SE_base[i].real() << '\t';
		// ofile << erg_IV.signal_GE_base[i].real() << '\t' << erg_IV.signal_SE_base[i].real() << '\t';
		// ofile << erg_EV.signal_GE_base[i].real() << '\t' << erg_EV.signal_SE_base[i].real() ;
		ofile << std::endl;
	}
    return 0;
}




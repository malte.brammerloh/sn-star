#!/bin/bash

Bfield=3;

#Echo time
TE=30; #in msec

#Gradients
Gx=50;

#B-field orientation angle
phi_angle=90;
omega_angle=45;

#timing activation 
O2idx=2;
Flowidx=2;

#timing baseline
O2baseidx=1
Flowbaseidx=1;

#ROI
ROIx1=0;
ROIx2=725;
ROIy1=0;
ROIy2=725;
ROIz1=0;
ROIz2=400;
            
    
    
    eval $(printf "pbsubmit -q max100 -n 8 -c \'./run_MCprotonA_Hybrid7.sh /usr/pubsw/common/matlab/7.11/ %1.1f %d %d %d %d %d %d %d %d %d %d /autofs/cluster/bartman/2/users/lgagnon/2013/mouse_20100203/synthetic_pO2/20100203_synthetic_rOC_1.0_rCBF_1.2.mat        %d      /autofs/cluster/bartman/2/users/lgagnon/2013/mouse_20100203/synthetic_pO2/20100203_synthetic_rCBF_1.2_vol.mat   %d    /autofs/cluster/bartman/2/users/lgagnon/2013/mouse_20100203/synthetic_pO2/20100203_synthetic_rOC_1.0_rCBF_1.2.mat        %d     %d'\n " $Bfield $phi_angle $omega_angle $TE $Gx $ROIx1 $ROIx2 $ROIy1 $ROIy2 $ROIz1 $ROIz2 $O2idx $Flowidx $O2baseidx $Flowbaseidx)  
    sleep 2
    
    eval $(printf "pbsubmit -q max100 -n 8 -c \'./run_MCprotonA_Hybrid7.sh /usr/pubsw/common/matlab/7.11/ %1.1f %d %d %d %d %d %d %d %d %d %d /autofs/cluster/bartman/2/users/lgagnon/2013/mouse_20100203/synthetic_pO2/20100203_synthetic_rOC_1.0_rCBF_1.4.mat        %d      /autofs/cluster/bartman/2/users/lgagnon/2013/mouse_20100203/synthetic_pO2/20100203_synthetic_rCBF_1.4_vol.mat   %d    /autofs/cluster/bartman/2/users/lgagnon/2013/mouse_20100203/synthetic_pO2/20100203_synthetic_rOC_1.0_rCBF_1.4.mat        %d     %d'\n " $Bfield $phi_angle $omega_angle $TE $Gx $ROIx1 $ROIx2 $ROIy1 $ROIy2 $ROIz1 $ROIz2 $O2idx $Flowidx $O2baseidx $Flowbaseidx)  
    sleep 2
    
    eval $(printf "pbsubmit -q max100 -n 8 -c \'./run_MCprotonA_Hybrid7.sh /usr/pubsw/common/matlab/7.11/ %1.1f %d %d %d %d %d %d %d %d %d %d /autofs/cluster/bartman/2/users/lgagnon/2013/mouse_20100203/synthetic_pO2/20100203_synthetic_rOC_1.0_rCBF_1.6.mat        %d      /autofs/cluster/bartman/2/users/lgagnon/2013/mouse_20100203/synthetic_pO2/20100203_synthetic_rCBF_1.6_vol.mat   %d    /autofs/cluster/bartman/2/users/lgagnon/2013/mouse_20100203/synthetic_pO2/20100203_synthetic_rOC_1.0_rCBF_1.6.mat        %d     %d'\n " $Bfield $phi_angle $omega_angle $TE $Gx $ROIx1 $ROIx2 $ROIy1 $ROIy2 $ROIz1 $ROIz2 $O2idx $Flowidx $O2baseidx $Flowbaseidx)  
    sleep 2
    

    
    eval $(printf "pbsubmit -q max100 -n 8 -c \'./run_MCprotonA_Hybrid7.sh /usr/pubsw/common/matlab/7.11/ %1.1f %d %d %d %d %d %d %d %d %d %d /autofs/cluster/bartman/2/users/lgagnon/2013/mouse_20100203/synthetic_pO2/20100203_synthetic_rOC_1.1_rCBF_1.0.mat        %d      /autofs/cluster/bartman/2/users/lgagnon/2013/mouse_20100203/synthetic_pO2/20100203_synthetic_rCBF_1.0_vol.mat   %d    /autofs/cluster/bartman/2/users/lgagnon/2013/mouse_20100203/synthetic_pO2/20100203_synthetic_rOC_1.1_rCBF_1.0.mat        %d     %d'\n " $Bfield $phi_angle $omega_angle $TE $Gx $ROIx1 $ROIx2 $ROIy1 $ROIy2 $ROIz1 $ROIz2 $O2idx $Flowidx $O2baseidx $Flowbaseidx)  
    sleep 2

    eval $(printf "pbsubmit -q max100 -n 8 -c \'./run_MCprotonA_Hybrid7.sh /usr/pubsw/common/matlab/7.11/ %1.1f %d %d %d %d %d %d %d %d %d %d /autofs/cluster/bartman/2/users/lgagnon/2013/mouse_20100203/synthetic_pO2/20100203_synthetic_rOC_1.1_rCBF_1.2.mat        %d      /autofs/cluster/bartman/2/users/lgagnon/2013/mouse_20100203/synthetic_pO2/20100203_synthetic_rCBF_1.2_vol.mat   %d    /autofs/cluster/bartman/2/users/lgagnon/2013/mouse_20100203/synthetic_pO2/20100203_synthetic_rOC_1.1_rCBF_1.2.mat        %d     %d'\n " $Bfield $phi_angle $omega_angle $TE $Gx $ROIx1 $ROIx2 $ROIy1 $ROIy2 $ROIz1 $ROIz2 $O2idx $Flowidx $O2baseidx $Flowbaseidx)  
    sleep 2
    
    eval $(printf "pbsubmit -q max100 -n 8 -c \'./run_MCprotonA_Hybrid7.sh /usr/pubsw/common/matlab/7.11/ %1.1f %d %d %d %d %d %d %d %d %d %d /autofs/cluster/bartman/2/users/lgagnon/2013/mouse_20100203/synthetic_pO2/20100203_synthetic_rOC_1.1_rCBF_1.4.mat        %d      /autofs/cluster/bartman/2/users/lgagnon/2013/mouse_20100203/synthetic_pO2/20100203_synthetic_rCBF_1.4_vol.mat   %d    /autofs/cluster/bartman/2/users/lgagnon/2013/mouse_20100203/synthetic_pO2/20100203_synthetic_rOC_1.1_rCBF_1.4.mat        %d     %d'\n " $Bfield $phi_angle $omega_angle $TE $Gx $ROIx1 $ROIx2 $ROIy1 $ROIy2 $ROIz1 $ROIz2 $O2idx $Flowidx $O2baseidx $Flowbaseidx)  
    sleep 2
    
    eval $(printf "pbsubmit -q max100 -n 8 -c \'./run_MCprotonA_Hybrid7.sh /usr/pubsw/common/matlab/7.11/ %1.1f %d %d %d %d %d %d %d %d %d %d /autofs/cluster/bartman/2/users/lgagnon/2013/mouse_20100203/synthetic_pO2/20100203_synthetic_rOC_1.1_rCBF_1.6.mat        %d      /autofs/cluster/bartman/2/users/lgagnon/2013/mouse_20100203/synthetic_pO2/20100203_synthetic_rCBF_1.6_vol.mat   %d    /autofs/cluster/bartman/2/users/lgagnon/2013/mouse_20100203/synthetic_pO2/20100203_synthetic_rOC_1.1_rCBF_1.6.mat        %d     %d'\n " $Bfield $phi_angle $omega_angle $TE $Gx $ROIx1 $ROIx2 $ROIy1 $ROIy2 $ROIz1 $ROIz2 $O2idx $Flowidx $O2baseidx $Flowbaseidx)  
    sleep 2
    


    eval $(printf "pbsubmit -q max100 -n 8 -c \'./run_MCprotonA_Hybrid7.sh /usr/pubsw/common/matlab/7.11/ %1.1f %d %d %d %d %d %d %d %d %d %d /autofs/cluster/bartman/2/users/lgagnon/2013/mouse_20100203/synthetic_pO2/20100203_synthetic_rOC_1.2_rCBF_1.0.mat        %d      /autofs/cluster/bartman/2/users/lgagnon/2013/mouse_20100203/synthetic_pO2/20100203_synthetic_rCBF_1.0_vol.mat   %d    /autofs/cluster/bartman/2/users/lgagnon/2013/mouse_20100203/synthetic_pO2/20100203_synthetic_rOC_1.2_rCBF_1.0.mat        %d     %d'\n " $Bfield $phi_angle $omega_angle $TE $Gx $ROIx1 $ROIx2 $ROIy1 $ROIy2 $ROIz1 $ROIz2 $O2idx $Flowidx $O2baseidx $Flowbaseidx)  
    sleep 2

    eval $(printf "pbsubmit -q max100 -n 8 -c \'./run_MCprotonA_Hybrid7.sh /usr/pubsw/common/matlab/7.11/ %1.1f %d %d %d %d %d %d %d %d %d %d /autofs/cluster/bartman/2/users/lgagnon/2013/mouse_20100203/synthetic_pO2/20100203_synthetic_rOC_1.2_rCBF_1.4.mat        %d      /autofs/cluster/bartman/2/users/lgagnon/2013/mouse_20100203/synthetic_pO2/20100203_synthetic_rCBF_1.4_vol.mat   %d    /autofs/cluster/bartman/2/users/lgagnon/2013/mouse_20100203/synthetic_pO2/20100203_synthetic_rOC_1.2_rCBF_1.4.mat        %d     %d'\n " $Bfield $phi_angle $omega_angle $TE $Gx $ROIx1 $ROIx2 $ROIy1 $ROIy2 $ROIz1 $ROIz2 $O2idx $Flowidx $O2baseidx $Flowbaseidx)  
    sleep 2
    
    eval $(printf "pbsubmit -q max100 -n 8 -c \'./run_MCprotonA_Hybrid7.sh /usr/pubsw/common/matlab/7.11/ %1.1f %d %d %d %d %d %d %d %d %d %d /autofs/cluster/bartman/2/users/lgagnon/2013/mouse_20100203/synthetic_pO2/20100203_synthetic_rOC_1.2_rCBF_1.6.mat        %d      /autofs/cluster/bartman/2/users/lgagnon/2013/mouse_20100203/synthetic_pO2/20100203_synthetic_rCBF_1.6_vol.mat   %d    /autofs/cluster/bartman/2/users/lgagnon/2013/mouse_20100203/synthetic_pO2/20100203_synthetic_rOC_1.2_rCBF_1.6.mat        %d     %d'\n " $Bfield $phi_angle $omega_angle $TE $Gx $ROIx1 $ROIx2 $ROIy1 $ROIy2 $ROIz1 $ROIz2 $O2idx $Flowidx $O2baseidx $Flowbaseidx)  
    sleep 2



    eval $(printf "pbsubmit -q max100 -n 8 -c \'./run_MCprotonA_Hybrid7.sh /usr/pubsw/common/matlab/7.11/ %1.1f %d %d %d %d %d %d %d %d %d %d /autofs/cluster/bartman/2/users/lgagnon/2013/mouse_20100203/synthetic_pO2/20100203_synthetic_rOC_1.3_rCBF_1.0.mat        %d      /autofs/cluster/bartman/2/users/lgagnon/2013/mouse_20100203/synthetic_pO2/20100203_synthetic_rCBF_1.0_vol.mat   %d    /autofs/cluster/bartman/2/users/lgagnon/2013/mouse_20100203/synthetic_pO2/20100203_synthetic_rOC_1.3_rCBF_1.0.mat        %d     %d'\n " $Bfield $phi_angle $omega_angle $TE $Gx $ROIx1 $ROIx2 $ROIy1 $ROIy2 $ROIz1 $ROIz2 $O2idx $Flowidx $O2baseidx $Flowbaseidx)  
    sleep 2

    eval $(printf "pbsubmit -q max100 -n 8 -c \'./run_MCprotonA_Hybrid7.sh /usr/pubsw/common/matlab/7.11/ %1.1f %d %d %d %d %d %d %d %d %d %d /autofs/cluster/bartman/2/users/lgagnon/2013/mouse_20100203/synthetic_pO2/20100203_synthetic_rOC_1.3_rCBF_1.2.mat        %d      /autofs/cluster/bartman/2/users/lgagnon/2013/mouse_20100203/synthetic_pO2/20100203_synthetic_rCBF_1.2_vol.mat   %d    /autofs/cluster/bartman/2/users/lgagnon/2013/mouse_20100203/synthetic_pO2/20100203_synthetic_rOC_1.3_rCBF_1.2.mat        %d     %d'\n " $Bfield $phi_angle $omega_angle $TE $Gx $ROIx1 $ROIx2 $ROIy1 $ROIy2 $ROIz1 $ROIz2 $O2idx $Flowidx $O2baseidx $Flowbaseidx)  
    sleep 2
    
    eval $(printf "pbsubmit -q max100 -n 8 -c \'./run_MCprotonA_Hybrid7.sh /usr/pubsw/common/matlab/7.11/ %1.1f %d %d %d %d %d %d %d %d %d %d /autofs/cluster/bartman/2/users/lgagnon/2013/mouse_20100203/synthetic_pO2/20100203_synthetic_rOC_1.3_rCBF_1.4.mat        %d      /autofs/cluster/bartman/2/users/lgagnon/2013/mouse_20100203/synthetic_pO2/20100203_synthetic_rCBF_1.4_vol.mat   %d    /autofs/cluster/bartman/2/users/lgagnon/2013/mouse_20100203/synthetic_pO2/20100203_synthetic_rOC_1.3_rCBF_1.4.mat        %d     %d'\n " $Bfield $phi_angle $omega_angle $TE $Gx $ROIx1 $ROIx2 $ROIy1 $ROIy2 $ROIz1 $ROIz2 $O2idx $Flowidx $O2baseidx $Flowbaseidx)  
    sleep 2
    
    eval $(printf "pbsubmit -q max100 -n 8 -c \'./run_MCprotonA_Hybrid7.sh /usr/pubsw/common/matlab/7.11/ %1.1f %d %d %d %d %d %d %d %d %d %d /autofs/cluster/bartman/2/users/lgagnon/2013/mouse_20100203/synthetic_pO2/20100203_synthetic_rOC_1.3_rCBF_1.6.mat        %d      /autofs/cluster/bartman/2/users/lgagnon/2013/mouse_20100203/synthetic_pO2/20100203_synthetic_rCBF_1.6_vol.mat   %d    /autofs/cluster/bartman/2/users/lgagnon/2013/mouse_20100203/synthetic_pO2/20100203_synthetic_rOC_1.3_rCBF_1.6.mat        %d     %d'\n " $Bfield $phi_angle $omega_angle $TE $Gx $ROIx1 $ROIx2 $ROIy1 $ROIy2 $ROIz1 $ROIz2 $O2idx $Flowidx $O2baseidx $Flowbaseidx)  
    sleep 2


#done








